// ChinesePoker.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "ChinesePoker.h"

// AI Strategy ideas:
// Power ranking system to figure out strength of hand in comparison to potential hands by other players
// --> Keep highly ranked hands from being played early
// Determining guaranteed or likely freebies to sequence hands so weak cards aren't left to be played last

bool isSameCard(cardObject card1, cardObject card2)
{
    return ((card1.getValue() == card2.getValue()) && (card1.getSuit() == card2.getSuit()));
}

cardObject getRandomCard()
{
    cardObject randomCard;
    randomCard.setValues(std::rand() % 13, std::rand() % 4);
    return randomCard;
}

void dealCards()
{
    cardObject deck[52];
    auto deckSize = (sizeof(deck) / sizeof(cardObject));

    for (int i = 0; i < deckSize; i++)
    {
        bool insertCard = true;

        cardObject newCard = getRandomCard();
        for (int j = 0; j < i; j++)
        {
            if (isSameCard(newCard, deck[j]))
            {
                i--;
                insertCard = false;
                break;
            }
        }
        
        if (insertCard)
            deck[i] = newCard;
    }

    for (int i = 0; i < deckSize; i++)
    {
        if (i < deckSize / 4)
            globalHands::hand1.push_back(deck[i]);
        else if (i < (deckSize / 4) * 2)
            globalHands::hand2.push_back(deck[i]);
        else if (i < (deckSize / 4) * 3)
            globalHands::hand3.push_back(deck[i]);
        else
            globalHands::hand4.push_back(deck[i]);
    }

    sortHand(globalHands::hand1);
    sortHand(globalHands::hand2);
    sortHand(globalHands::hand3);
    sortHand(globalHands::hand4);
}

void printHand(int handNumber)
{
    switch (handNumber)
    {
    case 1:
    {
        for (int i = 0; i < globalHands::hand1.size(); i++)
            std::cout << globalHands::hand1.at(i).getValueName() << " of " << globalHands::hand1.at(i).getSuitName() << " | ";
        break;
    }
    default:
        std::cout << "Not implemented yet";
    }
    std::cout << std::endl;
}

void printHand(std::vector<cardObject> hand)
{
    for (int i = 0; i < hand.size(); i++)
        std::cout << hand.at(i).getValueName() << " of " << hand.at(i).getSuitName() << " | ";
    std::cout << std::endl;
}

cardObject processCard(std::string input, std::vector<cardObject> hand)
{
    std::string rankStr = input.substr(0, 1);
    int rank;
    std::string suit;
    int sInt;

    try
    {
        rank = std::stoi(input.substr(0, 2)); // check for 10
        if (rank == 10)
            suit = input.substr(2, 1);
        else
            suit = input.substr(1, 1);
        rank -= 2; // correct for true values
    }
    catch (const std::invalid_argument& ia)
    {
        try {
            rank = std::stoi(rankStr);
            rank -= 2; // correct for true values
        }
        catch (const std::invalid_argument &ia)
        {
            if (!rankStr.compare("j") || !rankStr.compare("J"))
                rank = 9;
            else if (!rankStr.compare("q") || !rankStr.compare("Q"))
                rank = 10;
            else if (!rankStr.compare("k") || !rankStr.compare("K"))
                rank = 11;
            else if (!rankStr.compare("a") || !rankStr.compare("A"))
                rank = 12;
            else
                rank = -1;

            suit = input.substr(1, 1);
        }
    }

    if (!suit.compare("d") || !suit.compare("D"))
        sInt = 0;
    else if (!suit.compare("c") || !suit.compare("C"))
        sInt = 1;
    else if (!suit.compare("h") || !suit.compare("H"))
        sInt = 2;
    else if (!suit.compare("s") || !suit.compare("S"))
        sInt = 3;
    else
        sInt = -1; // invalid input

    cardObject newCard;
    newCard.setValues(rank, sInt);
    if (!handContainsCard(hand, newCard))
        newCard.setValues(-1, -1);
    return newCard;
}

comboTypes isValidTurn(std::vector<cardObject> turn, bool starting, cardTypes validTypes)
{
    std::vector<comboTypes> foundHands;
    foundHands.push_back(ROYAL_FLUSH);
    foundHands.push_back(BOMB);
    foundHands.push_back(FULL_HOUSE);
    foundHands.push_back(STRAIGHT);
    foundHands.push_back(FLUSH);

    cardObject startingCard;
    startingCard.setValues(1, 0);

    if (turn.size() < 1)
        return INVALID;

    if (starting && !handContainsCard(turn, startingCard)) // make sure 3 of diamonds is played if starting
        return INVALID;

    int tempSuit = turn.at(0).getSuit();
    int tempRank = turn.at(0).getValue();
    int identicalCards = 0;
    bool foundDouble = false;
    bool foundTriple = false;
    bool foundQuad = false;
    
    if (validTypes == FREEBIE)
    {
        if (turn.size() == 5)
            validTypes = COMBO;
        else
            validTypes = (cardTypes)(turn.size() - 1);
    }

    switch (validTypes)
    {
    case SINGLE:
        return (comboTypes)(turn.size() == 1);
    case DOUBLE:
        return (comboTypes)(turn.size() == 2 && (turn.at(0).getValue() == turn.at(1).getValue()));
    case TRIPLE:
        return (comboTypes)(turn.size() == 3 && (turn.at(0).getValue() == turn.at(1).getValue()) && ((turn.at(1).getValue() == turn.at(2).getValue())));
    case COMBO:
        for (int i = 1; i < turn.size(); i++)
        {
            if (turn.at(i).getSuit() != tempSuit && vectorIndexOf(foundHands, FLUSH) != -1) // flush check
                foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, FLUSH));

            if (turn.at(i).getValue() != tempRank + 1 && vectorIndexOf(foundHands, STRAIGHT) != -1) // straight check
                foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, STRAIGHT));
            else
                tempRank++;

            if (turn.at(i).getValue() == turn.at((size_t)i - 1).getValue())
            {
                identicalCards++;
                if (identicalCards == 1)
                    identicalCards++;
            }
            else
            {
                if (identicalCards == 2)
                    foundDouble = true;
                else if (identicalCards == 3)
                    foundTriple = true;
                else if (identicalCards == 4)
                    foundQuad = true;
                identicalCards = 0;
            }
        }

        if (identicalCards == 2)
            foundDouble = true;
        else if (identicalCards == 3)
            foundTriple = true;
        else if (identicalCards == 4)
            foundQuad = true;
        identicalCards = 0;

        if (!(foundDouble && foundTriple)) // full house check
            foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, FULL_HOUSE));
        if (!foundQuad) // 4 of a kind(bomb) check
            foundHands.erase(foundHands.begin() + vectorIndexOf(foundHands, BOMB));

        if (!(vectorIndexOf(foundHands, FLUSH) != -1 && vectorIndexOf(foundHands, STRAIGHT) != -1)) // royal flush
            foundHands.erase(foundHands.begin());

        if (foundHands.size() > 0)
            return foundHands.front();
        else
            return INVALID;
    default:
        return INVALID;
    }
}

cardTypes humanTurn(std::vector<cardObject> *hand, bool starting, cardTypes validTypes)
{
    comboTypes validTurn = INVALID;
    int numCards;
    std::vector<cardObject> attemptedTurn;
    std::string cardString;
    size_t turnSize;

    if (starting)
        std::cout << "\nYou are starting. You can play any valid hand that includes the 3 of diamonds.\n";
    else if (validTypes == FREEBIE)
        std::cout << "\nAll computers passed so you have a freebie. You can play any valid hand.\n";
    else
    {
        std::cout << "\nHand to beat: " << std::endl;
        printHand(globalHands::handHistory->back());
    }

    std::cout << "Your hand: " << std::endl;
    printHand(1);

    while (!validTurn)
    {
        std::cout << "\nEnter in the number of cards you want to play. If you want to pass, enter 0.";
        std::cin >> numCards;

        if (numCards == 0)
            if (validTypes == FREEBIE)
                std::cout << "\nYou can't pass on your own freebie. You can play any hand or card you'd like.";
            else
                return PASS;

        while (!(numCards > 0 && numCards <= 5 && numCards != 4))
        {
            std::cout << "\nNumber of cards must be 1, 2, 3, or 5.\n";
            std::cin >> numCards;
        }

        std::cout << "\nEnter in the first card you want to play.\nUse the format: number/first letter of face, first letter of suit. For example, 3d = 3 of diamonds, qs = queen of spades.\n";
        std::cin >> cardString;
        cardObject processedCard = processCard(cardString, *hand);

        while (processedCard.getSuit() == -1 || processedCard.getValue() == -1) // invalid input
        {
            std::cout << "Invalid input, try again.\n";
            std::cin >> cardString;
            processedCard = processCard(cardString, *hand);
        }

        attemptedTurn.push_back(processedCard);

        for (int i = 0; i < numCards - 1; i++)
        {
            std::cout << "Enter in the next card you want to play.\n";
            std::cin >> cardString;
            processedCard = processCard(cardString, *hand);

            while (processedCard.getSuit() == -1 || processedCard.getValue() == -1 || handContainsCard(attemptedTurn, processedCard)) // invalid input
            {
                std::cout << "Invalid input, try again.\n";
                std::cin >> cardString;
                processedCard = processCard(cardString, *hand);
            }

            attemptedTurn.push_back(processedCard);
        }

        validTurn = isValidTurn(attemptedTurn, starting, validTypes);
        if (!validTurn)
            std::cout << "The cards you entered do not form a valid hand. Try again.\n";
        else
        {
            std::cout << "You played a ";
            switch (validTurn)
            {
            case NON_COMBO:
                switch (attemptedTurn.size())
                {
                case 1:
                    std::cout << "single card." << std::endl;
                    break;
                case 2:
                    std::cout << "double." << std::endl;
                    break;
                case 3:
                    std::cout << "triple." << std::endl;
                    break;
                }
                break;
            case FLUSH:
                std::cout << "flush." << std::endl;
                break;
            case STRAIGHT:
                std::cout << "straight." << std::endl;
                break;
            case FULL_HOUSE:
                std::cout << "full house." << std::endl;
                break;
            case BOMB:
                std::cout << "bomb." << std::endl;
                break;
            case ROYAL_FLUSH:
                std::cout << "ROYAL FLUSH." << std::endl;
                break;
            }
            if (!starting && globalHands::handHistory && validTypes != FREEBIE)
            {
                if (attemptedTurn < globalHands::handHistory->back())
                {
                    std::cout << "The hand you played doesn't beat the previous hand. Try again or pass." << std::endl;
                    validTurn = INVALID;
                    continue;
                }
            }
            else if (!globalHands::handHistory) // this should only happen if the player is starting
                globalHands::handHistory = new std::vector<std::vector<cardObject>>;

            globalHands::handHistory->push_back(attemptedTurn);

            for (size_t i = 0; i < attemptedTurn.size(); i++)
                hand->erase(hand->begin() + vectorIndexOf(*hand, attemptedTurn.at(i))); // remove cards from player's hand
            
            turnSize = attemptedTurn.size();

            std::cout << std::endl << std::endl << std::endl << "--------------------------------------------" << std::endl;
        }
        attemptedTurn.clear();
    }

    return (validTurn > NON_COMBO) ? COMBO : (cardTypes)(turnSize - 1); // convert hand result back to cardTypes
}

cardTypes computerTurn(std::vector<cardObject>* hand, bool starting, cardTypes validTypes)
{
    std::vector<cardObject> lowHand;
    if (starting) // error stems from not detecting doubles or triples yet
        lowHand = takeStartingTurn(*hand);
    else if (validTypes == FREEBIE) // set ignoreHistory to true
        lowHand = lowestValidHandOfType(*hand, validTypes, true, false);
    else
        lowHand = lowestValidHandOfType(*hand, validTypes, false, true);
    if (!lowHand.size())
        lowHand = lowestValidHandOfType(*hand, validTypes); // only cards left are 2s or no valid hands available

    comboTypes turnType = isValidTurn(lowHand, starting, validTypes);

    if (!lowHand.size())
        return PASS;
    else if (lowHand.size() == (validTypes == COMBO) ? 5 : validTypes + 1) // correct turn size
    {
        if (turnType == INVALID)
        {
            std::cout << "Invalid turn attempt by computer." << std::endl;
            throw 1;
        }
        for (size_t i = 0; i < lowHand.size(); i++)
            hand->erase(hand->begin() + vectorIndexOf(*hand, lowHand.at(i)));
        globalHands::handHistory->push_back(lowHand);
    }
    else
    {
        std::cout << "Invalid turn size by computer (" << std::to_string(lowHand.size()) << " cards)." << std::endl;
        throw 2;
    }
    return (turnType > NON_COMBO) ? COMBO : (cardTypes)(lowHand.size() - 1);
}

int findStartingPlayer(std::vector<std::vector<cardObject> *> hands)
{
    for (int i = 0; i < hands.size(); i++)
        for (int j = 0; j < hands.at(i)->size(); j++)
            if (hands.at(i)->at(j).getValue() == 1 && hands.at(i)->at(j).getSuit() == 0)
                return i + 1;
    return 0;
}

void runGame(int numPlayers)
{
    std::vector<std::vector<cardObject>*> allHands;
    allHands.push_back(&globalHands::hand1);
    allHands.push_back(&globalHands::hand2);
    allHands.push_back(&globalHands::hand3);
    allHands.push_back(&globalHands::hand4);

    int startingPlayer = findStartingPlayer(allHands);
    cardTypes validTypes = cardTypes::FREEBIE;

    if (startingPlayer == 1) // in the future put this as range for number of human players
        validTypes = humanTurn(allHands.at((size_t)startingPlayer - 1), true, validTypes);
    else
    {
        validTypes = computerTurn(allHands.at((size_t)startingPlayer - 1), true, validTypes);
        std::cout << "Computer " + std::to_string(startingPlayer - 1) + " played:" << std::endl;
        printHand(globalHands::handHistory->back());
    }

    bool firstTurn = true;
    bool isPassed[4] = { false, false, false, false };
    startingPlayer--; // startingPlayer now matches hand index
    while (true)
    {
        for (int i = 0; i < numPlayers; i++)
        {
            if (firstTurn && startingPlayer == i)
                continue;

            bool shouldFreebie = true;
            for (int j = 0; j < 4; j++)
                if (j != i && !isPassed[j])
                    shouldFreebie = false;

            if (shouldFreebie)
                validTypes = FREEBIE;

            cardTypes backupType = validTypes;

            if (!isPassed[i])
                validTypes = humanTurn(allHands.at(i), false, validTypes);
            else
                continue;

            if (firstTurn)
                firstTurn = false;

            if (validTypes == PASS)
            {
                isPassed[i] = true;
                validTypes = backupType;
            }
            else if (shouldFreebie)
                for (int j = 0; j < 4; j++)
                    isPassed[j] = false;
            else
                isPassed[i] = false;

            if (!allHands.at(i)->size())
            {
                std::cout << "\nPlayer " << i + 1 << " wins.\n";
                goto gameOver;
            }
        }
        for (int i = numPlayers; i < 4; i++)
        {
            if (firstTurn && startingPlayer == i)
                continue;

            bool shouldFreebie = true;
            for (int j = 0; j < 4; j++)
                if (j != i && !isPassed[j])
                    shouldFreebie = false;

            if (shouldFreebie)
                validTypes = FREEBIE;

            cardTypes backupType = validTypes;

            if (!isPassed[i])
                validTypes = computerTurn(allHands.at(i), false, validTypes);
            else
                continue;
            
            if (firstTurn)
                firstTurn = false;

            if (validTypes == PASS)
            {
                isPassed[i] = true;
                validTypes = backupType;
            }
            else if (shouldFreebie)
                for (int j = 0; j < 4; j++)
                    isPassed[j] = false;
            else
                isPassed[i] = false;


            if (!isPassed[i])
            {
                std::cout << "Computer " + std::to_string(i) + " played:" << std::endl;
                printHand(globalHands::handHistory->back());
            }
            else
                std::cout << "Computer " + std::to_string(i) + " passed." << std::endl;

            if (!allHands.at(i)->size())
            {
                std::cout << "\nComputer " << i << " wins.\n";
                goto gameOver;
            }
        }

    }
gameOver:
    std::cout << "Play again? (Y/N)";
    std::string response;
    std::cin >> response;
}

int main()
{
    std::srand(std::time(0));

    dealCards();

    int numPlayers;
    std::cout << "Enter number of human players (between 0 and 4):";
    std::cin >> numPlayers;
    while (!(numPlayers >= 0 && numPlayers <= 4))
    {
        std::cout << "\nValue must be an integer between 0 and 4:";
        std::cin >> numPlayers;
    }

    runGame(1/*numPlayers*/);
}

bool handContainsCard(std::vector<cardObject> hand, cardObject card)
{
    for (int i = 0; i < hand.size(); i++)
        if (isSameCard(card, hand.at(i)))
            return true;
    return false;
}

bool handContainsValue(std::vector<cardObject> hand, cardObject card)
{
    for (int i = 0; i < hand.size(); i++)
        if (card.getValue() == hand.at(i).getValue())
            return true;
    return false;
}

int vectorIndexOf(std::vector<comboTypes> input, comboTypes element)
{
    for (int i = 0; i < input.size(); i++)
        if (input.at(i) == element)
            return i;
    return -1;
}

int vectorIndexOf(std::vector<cardObject> hand, cardObject element)
{
    for (int i = 0; i < hand.size(); i++)
        if (isSameCard(element, hand.at(i)))
            return i;
    return -1;
}

// assuming hand history exists, will have a separate function if starting
// also assuming all hands in hand history are valid
// will not exclude cards that make up a double/triple, this will be done in the computers turn itself
std::vector<cardObject> lowestValidHandOfType(std::vector<cardObject> hand, cardTypes validTypes, bool ignoreHistory, bool ignoreTwos)
{
    assert(globalHands::handHistory);
    //assert(validTypes != FREEBIE);

    std::vector<cardObject> returnHand = std::vector<cardObject>();
    std::vector<cardObject> storeRoyalFlush = std::vector<cardObject>();

    std::map<int, int> sameRanks, sameSuits;

    auto it = hand.begin();

    while (it != hand.end())
    {
        sameRanks[it->getValue()]++;
        sameSuits[it->getSuit()]++;

        if (ignoreTwos && it->getValue() == 0)
            it = hand.erase(it);
        else
            it++;
    }

    if (!hand.size()) // only cards left are twos, let calling function do checks for that and recall
        return returnHand;

    if (validTypes == FREEBIE) // play most amount of cards possible for now, can be improved upon with "power ranking" system
    {
        cardTypes recursiveType = FREEBIE;
        bool recursiveIgnoreTwos = true;
        while (!returnHand.size() && recursiveType >= SINGLE) // use recursion to loop through validTypes
        {
            recursiveType = (cardTypes)(recursiveType - 1);
            returnHand = lowestValidHandOfType(hand, recursiveType, recursiveIgnoreTwos);

            if (recursiveType == SINGLE && !returnHand.size())
            {
                recursiveIgnoreTwos = false; // no hands to play that don't have twos, so play a twos hand, loop back through
                recursiveType = FREEBIE;
            }
        }
    }

    if (validTypes < COMBO) // single, double, or triple
    {
        size_t idx = 0;

        // skip all cards that are less than previous hand and that are not contained in the correct hand size
        while (idx < hand.size())
        {
                if (!(sameRanks.find(hand.at(idx).getValue())->second == validTypes + 1) || (cardIsInCombo(hand, hand.at(idx)) && validTypes == SINGLE) 
                    || (hand.at(idx) < globalHands::handHistory->back().back() && !ignoreHistory)) // validTypes + 1 gives hand size
                    idx++;
                else
                    break;
        }
        
        // add correct number of calls to returnHand
        if (idx + validTypes < hand.size())
            for (size_t i = idx + validTypes; i >= idx && i < hand.size(); i--) // must check for i in bounds despite looping backwards because the unsigned int will roll over to max value
                returnHand.push_back(hand.at(i));

    }
    else // combo, work from lowest to highest
    {
        // TODO: make optimal version that returns hand that breaks up the least other combos instead of just the lowest
        comboTypes prevType = isValidTurn(globalHands::handHistory->back(), false, FREEBIE);

        // lowest straight detection
        if (prevType <= STRAIGHT || ignoreHistory)
        {
            bool inStraight = false;
            int straightLength = 0;
            size_t startIdx = -1;
            int doublesIncluded = 0;
            for (size_t i = 0; i < hand.size(); i++)
            {
                if (i > 0) // straight check
                {
                    if (doublesIncluded > 2) // dont break up more than 2 doubles
                        break;

                    if (hand.at(i).getValue() - hand.at(i - 1).getValue() <= 1)
                    {
                        if (sameRanks.find(hand.at(i).getValue())->second > 2) // dont break up triples or quads
                        {
                            inStraight = false;
                            startIdx = -1;
                            straightLength = 0;
                            if ((i + (size_t)sameRanks.find(hand.at(i).getValue())->second - 1) < hand.size())
                                i += (size_t)sameRanks.find(hand.at(i).getValue())->second - 1;
                            continue;
                        }
                        else if (sameRanks.find(hand.at(i).getValue())->second == 2)
                            doublesIncluded++;

                        inStraight = true;
                        if (startIdx == -1)
                            startIdx = i - 1;
                        if (hand.at(i).getValue() - hand.at(i - 1).getValue() == 1)
                            straightLength++;
                        if (straightLength == 1)
                            straightLength++;
                    }
                    else if (inStraight && straightLength >= 5
                        && (ignoreHistory ||
                            (prevType != STRAIGHT || globalHands::handHistory->back().back() < hand.at(i - 1))))
                    {
                        for (size_t j = startIdx; j < i; j++)
                            if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                                returnHand.push_back(hand.at(j));
                    }
                    else
                    {
                        inStraight = false;
                        straightLength = 0;
                        startIdx = -1;
                    }
                }
            }
            // check again at end
            if (inStraight && straightLength >= 5
                && (ignoreHistory ||
                    (prevType != STRAIGHT || globalHands::handHistory->back().back() < hand.back())))
            {
                for (size_t j = startIdx; j < hand.size(); j++)
                    if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                        returnHand.push_back(hand.at(j));
            }
            if (returnHand.size() == 5 && isValidTurn(returnHand, false, FREEBIE) == ROYAL_FLUSH)
            {
                storeRoyalFlush = returnHand;
                returnHand.clear();
            }
        }

        if (!returnHand.size() && (prevType <= FLUSH || ignoreHistory))
        {
            for (int suit = (!ignoreHistory && prevType == FLUSH) ? globalHands::handHistory->back().front().getSuit() : 0; suit < 4; suit++) // flush
            {
                if (!sameSuits[suit]) // if there are no cards of this suit, checking for second will give an exception so you need to nullptr check
                    continue;
                if (sameSuits.find(suit)->second >= 5) // found lowest flush
                {
                    for (int i = 0; i < hand.size(); i++)
                        // only add stray cards first
                        if (hand.at(i).getSuit() == suit && !cardIsInCombo(hand, hand.at(i), FLUSH))
                            returnHand.push_back(hand.at(i));

                    if (returnHand.size() < 5)
                        for (int i = 0; i < hand.size(); i++)
                            // on 2nd loop, only break up doubles
                            if (hand.at(i).getSuit() == suit && !(sameRanks.find(hand.at(i).getValue())->second > 2) && !handContainsCard(returnHand, hand.at(i)))
                                returnHand.push_back(hand.at(i));

                    if (returnHand.size() < 5) // breaking up a higher 5 card combo or triple, move on
                        returnHand.clear();
                    else if (isValidTurn(returnHand, false, FREEBIE) == ROYAL_FLUSH) // make sure its not a straight flush, this will be handled later
                    {
                        storeRoyalFlush = returnHand;
                        returnHand.clear();
                    }
                }
            }
        }
        if (returnHand < globalHands::handHistory->back() && !ignoreHistory)
            returnHand.clear();
        if (!returnHand.size() && (prevType <= FULL_HOUSE || ignoreHistory))
        {
            cardObject refCard;
            if (!ignoreHistory)
            {
                if (globalHands::handHistory->back().front().getValue() == globalHands::handHistory->back().at(1).getValue() && globalHands::handHistory->back().at(1).getValue() == globalHands::handHistory->back().at(2).getValue())
                    refCard = globalHands::handHistory->back().front();
                else
                    refCard = globalHands::handHistory->back().back();
            }

            for (size_t i = 0; i < hand.size(); i++)
            {
                if (sameRanks.find(hand.at(i).getValue())->second == 3 && strayDouble(hand, hand.at(i)) && returnHand.size() != 3 && (refCard < hand.at(i) || ignoreHistory) && returnHand.size() < 5)
                {
                    for (size_t j = i; j < i + 3; j++)
                        returnHand.push_back(hand.at(j));
                    i += 2;
                }
                else if (sameRanks.find(hand.at(i).getValue())->second == 2 && returnHand.size() != 2 && returnHand.size() < 5)
                {
                    for (size_t j = i; j < i + 2; j++)
                        returnHand.push_back(hand.at(j));
                    i++;
                }
                if (returnHand.size() == 5)
                    break;
            }

            if (returnHand.size() < 5)
                returnHand.clear();
        }
        if (!returnHand.size() && (prevType <= BOMB || ignoreHistory))
        {
            for (size_t i = 0; i < hand.size(); i++)
            {
                if (sameRanks.find(hand.at(i).getValue())->second == 4 && returnHand.size() != 4)
                    for (size_t j = i; j < i + 4; j++)
                        returnHand.push_back(hand.at(j));
                else if (!cardIsInCombo(hand, hand.at(i)) && returnHand.size() != 1 && returnHand.size() != 5) // dont break up anything if its not needed
                    returnHand.push_back(hand.at(i));

                if (returnHand.size() == 5 && (ignoreHistory || globalHands::handHistory->back() < returnHand))
                    break;
                else if (returnHand.size() == 5) // previous hand was a higher bomb, can't play this hand
                    returnHand.clear();
            }
            if (returnHand.size() == 4) // couldnt find 1 stray card, just break up lowest non combo instead
                for (size_t i = 0; i < hand.size(); i++)
                    if (!cardIsInCombo(hand, hand.at(i), NON_COMBO) && !handContainsCard(returnHand, hand.at(i)))
                        returnHand.push_back(hand.at(i));

            if (returnHand.size() < 5)
                returnHand.clear();
        }
        if (!(returnHand.size() == 5) && (prevType <= ROYAL_FLUSH || ignoreHistory) && storeRoyalFlush.size() == 5)
            returnHand = storeRoyalFlush;
    }

    sortHand(returnHand);
    return returnHand;
}

// INCOMPLETE
std::vector<cardObject> highestHandOfType(std::vector<cardObject> hand, cardTypes validTypes)
{
    return std::vector<cardObject>();
}

// hand must be that of the starting players
// INCOMPLETE
std::vector<cardObject> takeStartingTurn(std::vector<cardObject> hand)
{
    assert(hand.front().getValue() == 1 && hand.front().getSuit() == 0); // must have 3 of diamonds, hand must be sorted

    // cant just check lowest hands because the three of diamonds might be contained in a higher hand

    std::vector<cardObject> returnHand = std::vector<cardObject>();
    returnHand.push_back(hand.front());

    globalHands::handHistory = new std::vector<std::vector<cardObject>>;

    if (!cardIsInCombo(hand, hand.front())) // isnt contained in any hand, play single card
        return returnHand;

    // for now, never include 2s in starting hand
    for (size_t i = 0; i < hand.size(); i++)
        if (hand.at(i).getValue() == 0)
            hand.erase(hand.begin() + i);

    // checking for hands is different here than in lowestValidHandOfType because you only have to check for hands that might contain the three of diamonds
    // starting with straight

    std::map<int, int> sameRanks, sameSuits;

    for (cardObject card : hand)
    {
        sameRanks[card.getValue()]++;
        sameSuits[card.getSuit()]++;
    }

    bool inStraight = false;
    int straightLength = 0;
    size_t startIdx = -1;
    int doublesIncluded = 0;
    for (size_t i = 0; i < hand.size(); i++)
    {
        if (i > 0) // straight check
        {
            if (doublesIncluded > 2) // dont break up more than 2 doubles
                break;

            if (hand.at(i).getValue() - hand.at(i - 1).getValue() <= 1)
            {
                if (sameRanks.find(hand.front().getValue())->second > 2) // dont break up triples or quads
                {
                    inStraight = false;
                    startIdx = -1;
                    straightLength = 0;
                    break;
                }
                else if (sameRanks.find(hand.at(i).getValue())->second == 2)
                    doublesIncluded++;

                inStraight = true;
                if (startIdx == -1)
                    startIdx = i - 1;
                if (hand.at(i).getValue() - hand.at(i - 1).getValue() == 1)
                    straightLength++;
                if (straightLength == 1)
                    straightLength++;
            }
            else if (inStraight && straightLength >= 5)
            {
                for (size_t j = startIdx; j < i; j++)
                    if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                        returnHand.push_back(hand.at(j));
            }
            else
            {
                inStraight = false;
                straightLength = 0;
                startIdx = -1;
                break;
            }
        }
    }
    // check again at end
    if (inStraight && straightLength >= 5)
    {
        for (size_t j = startIdx; j < hand.size(); j++)
            if (!handContainsValue(returnHand, hand.at(j)) && returnHand.size() < 5)
                returnHand.push_back(hand.at(j));
    }
    if (returnHand.size() == 5 && isValidTurn(returnHand, false, FREEBIE) == ROYAL_FLUSH) // obviously dont start a game with a royal flush <- rethink this
    {
        returnHand.clear();
    }
    else if (returnHand.size() == 5)
        return returnHand;
    else
        returnHand.clear();

    // now checking for flush
    std::vector<size_t> diamondCards = std::vector<size_t>();

    for (size_t i = 0; i < hand.size(); i++)
        if (hand.at(i).getSuit() == hand.front().getSuit())
            diamondCards.push_back(i);

    if (diamondCards.size() >= 5) // found flush
    {
        // first get rid of cards that are in other combos, loop backwards, keep 3 of diamonds
        for (size_t i = diamondCards.size() - 1; i > 0 && i < diamondCards.size(); i--) // must check for i < diamondCards.size() since it is an unsigned int and will roll over
            if (cardIsInCombo(hand, hand.at(diamondCards.at(i)), FLUSH) && diamondCards.size() > 5)
                diamondCards.erase(diamondCards.begin() + i);
    }

    while (diamondCards.size() > 5)
        diamondCards.pop_back();
    if (diamondCards.size() == 5)
    {
        for (size_t i = 0; i < diamondCards.size(); i++)
            returnHand.push_back(hand.at(diamondCards.at(i)));
        return returnHand;
    }
    else
        returnHand.clear();



    // unfinished

    return returnHand;
}

// assuming card is in hand, includes doubles and triples
// setting cExclude will make the function ignore that combo type
bool cardIsInCombo(std::vector<cardObject> hand, cardObject card, comboTypes cExclude)
{
    if (hand.size() == 1) // 1 card
        return false;

    if (cardIsInDbl(hand, card) && cExclude != NON_COMBO)
        return true;

    int sSuits = 0, sRanks = 0;
    bool inStraight = false, containsCard = false;
    int straightLength = 0;

    for (int i = 0; i < hand.size(); i++)
    {
        if (hand.at(i).getSuit() == card.getSuit() && !isSameCard(hand.at(i), card)) // count suits for flush
            sSuits++;
        if (hand.at(i).getValue() == card.getValue() && !isSameCard(hand.at(i), card)) // count suits for flush
            sRanks++;

        if (i > 0) // straight check
        {
            if (hand.at(i).getValue() - hand.at((size_t)i - 1).getValue() <= 1)
            {
                inStraight = true;
                if (hand.at(i).getValue() - hand.at((size_t)i - 1).getValue() == 1)
                    straightLength++;
                if (straightLength == 1)
                    straightLength++;
                if (isSameCard(hand.at(i), card) || (inStraight && i == 1 && isSameCard(hand.front(), card)))
                    containsCard = true;
            }
            else if (inStraight && containsCard && straightLength >= 5 && cExclude != STRAIGHT)
                return true;
            else
            {
                inStraight = false;
                containsCard = false;
                straightLength = 0;
            }
        }
    }

    // check again for straight, could be at the end of hand
    if (inStraight && containsCard && straightLength >= 5 && cExclude != STRAIGHT)
        return true;
    if (sSuits >= 5 && cExclude != FLUSH) // flush
        return true;

    if (cExclude != FULL_HOUSE && sRanks == 3 && strayDouble(hand, card))
        return true;
    if (cExclude != BOMB && sRanks == 4 && hand.size() > 4)
        return true;

    return false;
}

// also includes triples and quads
bool cardIsInDbl(std::vector<cardObject> hand, cardObject card)
{
    size_t cardIndex = vectorIndexOf(hand, card);

    if (hand.size() == 1) // 1 card
        return false;

    if (cardIndex < hand.size() - 1) // double/triple/quad check
        if (hand.at(cardIndex).getValue() == hand.at(cardIndex + 1).getValue())
            return true;
    if (cardIndex > 0) // double/triple/quad check
        if (hand.at(cardIndex).getValue() == hand.at(cardIndex - 1).getValue())
            return true;
    return false;
}

bool strayDouble(std::vector<cardObject> hand, cardObject excludeCard, bool triple)
{
    std::map<int, int> sameRanks;

    for (cardObject card : hand)
        sameRanks[card.getValue()]++;
    for (cardObject card : hand)
        if (sameRanks.find(card.getValue())->second == (triple ? 3 : 2) && card.getValue() != excludeCard.getValue())
            return true;
    return false;
}

bool operator<(std::vector<cardObject> hand1, std::vector<cardObject> hand2)
{
    std::vector<cardObject> sortedHand1 = hand1;
    std::vector<cardObject> sortedHand2 = hand2;
    sortHand(sortedHand1);
    sortHand(sortedHand2);
    comboTypes hand1Type = isValidTurn(hand1, false, FREEBIE);
    comboTypes hand2Type = isValidTurn(hand2, false, FREEBIE);
    cardObject refCard1;
    cardObject refCard2;
    if (hand1Type == hand2Type)
    {
        switch (hand1Type)
        {
        case NON_COMBO:
            if (hand1.size() != hand2.size())
                return (hand1.size() < hand2.size());
            else
                return (hand1.back() < hand2.back());
        case FLUSH:
            if (hand1.front().getSuit() != hand2.front().getSuit())
                return (hand1.front().getSuit() < hand2.front().getSuit());
            else
                return (hand1.back() < hand2.back());
        case FULL_HOUSE:
            if (hand1.front().getValue() == hand1.at(1).getValue() && hand1.at(1).getValue() == hand1.at(2).getValue())
                refCard1 = hand1.front();
            else
                refCard1 = hand1.back();
            if (hand2.front().getValue() == hand2.at(1).getValue() && hand2.at(1).getValue() == hand2.at(2).getValue())
                refCard2 = hand2.front();
            else
                refCard2 = hand2.back();
            return (refCard1 < refCard2);
        case BOMB:
            if (hand1.front().getValue() == hand1.at(1).getValue())
                refCard1 = hand1.front();
            else
                refCard1 = hand1.back();
            if (hand2.front().getValue() == hand2.at(1).getValue())
                refCard2 = hand2.front();
            else
                refCard2 = hand2.back();
            return (refCard1 < refCard2);
        default:
            return (hand1.back() < hand2.back());
        }
    }
    else
        return (hand1Type < hand2Type);
}

void sortHand(std::vector<cardObject> &hand)
{
    std::sort(hand.begin(), hand.end(), [](const cardObject& lhs, const cardObject& rhs) {
        return lhs < rhs;
        });
}