#include "cardObject.h"

void cardObject::setValues(int value, int suit)
{
    this->value = value;
    this->suit = suit;
}

int cardObject::getValue()
{
    return value;
}

int cardObject::getSuit()
{
    return suit;
}

std::string cardObject::getValueName()
{
    std::string name;
    if (value <= 8)
        name = std::to_string(value + 2);
    else
    {
        switch (value)
        {
        case 9:
            name = "Jack";
            break;
        case 10:
            name = "Queen";
            break;
        case 11:
            name = "King";
            break;
        case 12:
            name = "Ace";
            break;
        default:
            name = "invalid";
        }
    }

    return name;
}

std::string cardObject::getSuitName()
{
    switch (suit)
    {
    case 0:
        return "diamonds";
    case 1:
        return "clubs";
    case 2:
        return "hearts";
    case 3:
        return "spades";
    default:
        return "invalid parameter";
    }
}

bool operator<(const cardObject& card1, const cardObject& card2)
{
    if (card1.value != card2.value)
        if (card1.value == 0 || card2.value == 0)
            return((card1.value == card2.value) ? (card1.suit < card2.suit) : (card1.value > card2.value));
        else
            return (card1.value < card2.value);
    else
        return (card1.suit < card2.suit);
}