#pragma once
#include <string>

class cardObject
{
    int value, suit;
public:
    void setValues(int value, int suit);
    int getValue();
    int getSuit();
    std::string getSuitName();
    std::string getValueName();
    friend bool operator<(const cardObject& card1, const cardObject& card2);
};