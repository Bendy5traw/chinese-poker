#pragma once
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <cassert>
#include <iterator>

#include "cardObject.h"

namespace globalHands
{
    std::vector<cardObject> hand1;
    std::vector<cardObject> hand2;
    std::vector<cardObject> hand3;
    std::vector<cardObject> hand4;
    std::vector<std::vector<cardObject>>* handHistory;
}

enum cardTypes
{
    SINGLE,
    DOUBLE,
    TRIPLE,
    COMBO,
    FREEBIE,
    PASS
};

enum comboTypes
{
    INVALID,
    NON_COMBO,
    STRAIGHT,
    FLUSH,
    FULL_HOUSE,
    BOMB,
    ROYAL_FLUSH
};

// turn functions
// ---------------------------------------------------------------------------------------------
cardTypes humanTurn(std::vector<cardObject>* hand, bool starting, cardTypes validTypes);
cardTypes computerTurn(std::vector<cardObject>* hand, bool starting, cardTypes validTypes);
// ---------------------------------------------------------------------------------------------

// card/hand processing
// ---------------------------------------------------------------------------------------------
cardObject processCard(std::string input, std::vector<cardObject> hand);
comboTypes isValidTurn(std::vector<cardObject> turn, bool starting, cardTypes validTypes);
// ---------------------------------------------------------------------------------------------

// utility functions
// ---------------------------------------------------------------------------------------------
int findStartingPlayer(std::vector<std::vector<cardObject>*> hands);
bool handContainsCard(std::vector<cardObject> hand, cardObject card);
//bool handContainsCard(std::vector<cardObject>* hand, cardObject card);
bool handContainsValue(std::vector<cardObject> hand, cardObject card);
int vectorIndexOf(std::vector<comboTypes> input, comboTypes element);
int vectorIndexOf(std::vector<cardObject> hand, cardObject element);
void sortHand(std::vector<cardObject>& hand);
bool isSameCard(cardObject card1, cardObject card2);
// ---------------------------------------------------------------------------------------------

// computer turn functions
// ---------------------------------------------------------------------------------------------
std::vector<cardObject> lowestValidHandOfType(std::vector<cardObject> hand, cardTypes validTypes, bool ignoreHistory = false, bool ignoreTwos = false);
std::vector<cardObject> highestHandOfType(std::vector<cardObject> hand, cardTypes validTypes);
std::vector<cardObject> takeStartingTurn(std::vector<cardObject> hand);
bool cardIsInCombo(std::vector<cardObject> hand, cardObject card, comboTypes cExclude = INVALID);
bool cardIsInDbl(std::vector<cardObject> hand, cardObject card);
bool strayDouble(std::vector<cardObject> hand, cardObject excludeCard, bool triple = false);
// ---------------------------------------------------------------------------------------------

// operator overloads
// ---------------------------------------------------------------------------------------------
bool operator<(std::vector<cardObject> hand1, std::vector<cardObject> hand2);
// ---------------------------------------------------------------------------------------------